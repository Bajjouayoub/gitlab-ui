import * as description from './avatars_inline.md';

export default {
  followsDesignSystem: true,
  description,
};
